#!/bin/bash
WORK_DIR=/home/dsr/scripts
output=$(docker exec dsr-rabbitmq rabbitmqctl --vhost rabbitmq_vhost list_queues | grep reallocation)

echo "$(date) $output" >> "$WORK_DIR/rabbitmq.log"

